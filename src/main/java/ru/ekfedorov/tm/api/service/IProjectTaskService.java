package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllByProjectId(String projectId);

    Task bindTaskByProject(String projectId, String taskId);

    Task unbindTaskFromProject(String taskId);

    Project removeProjectById(String projectId);

}
