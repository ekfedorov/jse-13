package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findOneById(String id);

    Project removeOneById(String id);

    Project findOneByIndex(Integer index);

    Project removeOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByName(String name);

}
